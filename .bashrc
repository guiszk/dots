source ~/.git-prompt.sh
PROMPT_COMMAND=__prompt_command 

alias ls="ls -G"
alias la="ls -la"
alias ll="ls -l"
alias rm="rm -i"
alias pn='ping -c1 archlinux.org'

0x0() {
    if [[ $# -ne 1 ]]; then
        echo "0x0 [file|domain]"
        return 1
    else
    	if echo "$1" | grep -E -q "https?:\/\/(www.)?[a-zA-Z0-9]+(\.[a-zA-Z]{1,4})+\b"; then
    	    curl -F "shorten=${1}" http://0x0.st	
    	elif [ -f "$1" ]; then
    	    curl -F "file=@${1}" http://0x0.st
    	else
            echo "0x0 [file|domain]"
            return 1
    	fi 
    fi
}

transfer() { 
    if [ $# -eq 0 ]; then 
        echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md" 
        return 1
    fi
    tmpfile=$( mktemp -t transferXXX )
    if tty -s; then 
        basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g')
        curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile
    else 
        curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile
    fi
    cat $tmpfile
    rm -f $tmpfile
} 

gt () {
    if [ $# -lt 2 ] || [ $# -gt 3 ]; then
        echo "gt <commit message> <branch>"
        return 1
    fi
    if git rev-parse --is-inside-work-tree 2&>/dev/null; then
        git add *
        git commit -m $1
        if [ $# -eq 3 ]; then
            git push $2 master
        else
            git push origin master
        fi
    else
        echo "Not a git repo."
        return 1
    fi
}

__prompt_command() {
    local EXIT="$?"             
    PS1=""

    local reset='\[\e[0m\]'

    local red='\[\e[0;31m\]'
    local bold=$(tput bold)
    local normal=$(tput sgr0)

    if [ $EXIT != 0 ]; then
        PS1+="$red\[$bold\][$EXIT] \[$normal\]$reset"
    fi

    PS1+="\w$red\$(__git_ps1 ' [%s]')\[$normal\] $ $reset"
    #PS1+="\w \$ "
}