#!/bin/sh

if protonvpn s | grep -q Country; then
	echo $(protonvpn s | grep IP | tr -s " " | cut -d " " -f 2) \($(protonvpn s | grep Country | tr -s " " | cut -d " " -f 2-10)\)
else
	echo $(protonvpn s | grep IP | tr -s " " | cut -d " " -f 2) \($(protonvpn s | grep ISP | tr -s " " | cut -d " " -f 2-10)\)
fi
