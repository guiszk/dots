autoload -U colors && colors
autoload -Uz compinit
compinit

PS1="%(?..%B%{$fg[red]%}[%?] %{$reset_color%})%{$fg[grey]%}%n%{$reset_color%}@%{$fg[white]%}%m %{$fg[grey]%}%~ %{$reset_color%}%% "
#PS1="%{$fg[green]%}%n%{$reset_color%}@%{$fg[white]%}%m %{$fg[blue]%}%~ %{$reset_color%}%% "
alias ls='ls -G'
alias ll='ls -lh'
alias la='ls -lah'
alias l1='ls -1'
#alias sml='cd ~/git/SMLoadr; node SMLoadr -q FLAC -u'
alias pn='ping -c1 archlinux.org'
alias pw='pwgen -y -s $((12 + RANDOM % 20)) 1'

autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%F{red}[%b]%f'
zstyle ':vcs_info:*' enable git

sml () {
    HERE=$PWD
    if [ $# -ne 1 ]; then
        echo "sml <deezer link>"
        return 1
    fi
    if ! [ -d ~/git/SMLoadr ]; then
        echo "SMLoadr folder not found"
        return 1
    fi
    if ! [ -d /Volumes/Gui ]; then
        echo "Connect external HD."
        return 1
    fi
    cd ~/git/SMLoadr
    node SMLoadr -q FLAC -u $1
    cd $HERE
}

gt () {
    if [ $# -lt 1 ] || [ $# -gt 3 ]; then
        echo "gt [-n] [-m <commit message> <branch>]"
        return 1
    fi
    case "$1" in 
        "-n" ) 
            if git rev-parse --is-inside-work-tree 1&>/dev/null; then
                url=$(git config --get remote.origin.url)
                dirname=${PWD##*/}
                cd ..
                rm -rf $dirname
                git clone $url $dirname
                cd $dirname
            else
                echo "Not a git repo."
                return 1
            fi; shift;;
        "-m" )
            if git rev-parse --is-inside-work-tree 1&>/dev/null; then
                git add *
                git commit -m $2 
                if [ $# -eq 4 ]; then
                    git push $3 master
                else
                    git push origin master
                fi
            else
                echo "Not a git repo."
                return 1
            fi; shift;;
        *) echo "gt [-n] [-m <commit message> <branch>]"; exit 1;;
    esac
}

0x0 () {
    if [ $# -ne 1 ]; then
        echo "0x0 <link/file>"
        return 1
    fi
    regex='((https?)://)?[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]'
    if [ -f $1 ]; then
        curl -F'file=@'$1 http://0x0.st  
    elif [[ $1 =~ $regex ]]; then
        curl -F'shorten='$1 http://0x0.st
    fi
}

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/local/bin/vault vault
