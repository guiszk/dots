#!/bin/bash

mon="$(xrandr -q | grep -Eo "Virtual-1|Virtual1")"

if xrandr --query | grep -Eq "Virtual-1|Virtual1"; then
    if xrandr -q | grep -q "2560x1600"; then
        xrandr --output "$mon" --mode "2560x1600"
    elif xrandr -q | grep -q "1920x1080"; then
        xrandr --output "$mon" --mode "1920x1080"
    else
        xrandr --newmode "1920x1080"  173.00  1920 2048 2248 2576  1080 1083 1088 1120  -hsync +vsync
        xrandr --addmode "$mon" 1920x1080
        xrandr --output "$mon" --mode "1920x1080"
    fi
fi